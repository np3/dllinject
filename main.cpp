#include <iostream>
#include <exception>
#include <tchar.h>
#include <windows.h>
#include <TlHelp32.h>

// http://www.rohitab.com/discuss/topic/40409-codecave-injection-x64/
#ifdef _UNICODE
  typedef std::wstring tstring;
#else
  typedef std::string tstring;
#endif

using namespace std;

static const int DLLNAME_LEN = 100;
// x64 code variant
// Simple PIC code fragment
// !!! Use benefits of RIP addressing
// lea rcx, [rip + offset_libname]
// call [rip + offset_loadlibrary]
// xor rcx, rcx
// call [rip + offset_exitthread]
// Use https://defuse.ca/online-x86-assembler.htm#disassembly to get actual opcodes
#pragma pack(push, 1)
struct remCALL_PARAMETERS64
{
  BYTE dbgBreak;
  BYTE lea_rcx[3];
  DWORD libNameOffset;
  WORD  callLoadLib;
  DWORD loadLibOffset;
  WORD  xorEcx;
  WORD  callExitThread;
  DWORD exitThreadOffset;
  intptr_t loadLibAddr;
  intptr_t exitThreadAddr;
  BYTE     libName[DLLNAME_LEN + 1];
  remCALL_PARAMETERS64(bool needDebug = false) {
    dbgBreak = needDebug ? 0xCC : 0x90;
    lea_rcx[0] = 0x48; lea_rcx[1] = 0x8D; lea_rcx[2] = 0x0D;
    libNameOffset = offsetof(remCALL_PARAMETERS64, libName) - offsetof(remCALL_PARAMETERS64, callLoadLib); // 0x25
    callLoadLib = _byteswap_ushort(0xFF15);
    loadLibOffset = offsetof(remCALL_PARAMETERS64, loadLibAddr) - offsetof(remCALL_PARAMETERS64, xorEcx);
    xorEcx = _byteswap_ushort(0x31C9);
    callExitThread = _byteswap_ushort(0xFF15);
    exitThreadOffset = offsetof(remCALL_PARAMETERS64, exitThreadAddr) - offsetof(remCALL_PARAMETERS64, loadLibAddr);
    memset(libName, 0, sizeof(libName));
  }
};
#pragma pack(pop)

void printMsg(const char* pMsg)
{
  cout << pMsg << endl;
}

void printMsg(const _TCHAR *pMsg)
{
  cout << pMsg << endl;
}

void printHelp()
{
  cout << ("Usage: dllInject -P <Process name> -D <DLL Path> [-DBG]") << endl;
  cout << ("-DBG : start with 0x90 opcode (int 3)") << endl;
}

bool getPrivileges(void)
{
  HANDLE hToken;
  TOKEN_PRIVILEGES tokPriv;

  if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) {
    LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &tokPriv.Privileges[0].Luid);
    tokPriv.PrivilegeCount = 1;
    tokPriv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    return AdjustTokenPrivileges(hToken, 0, &tokPriv, sizeof(tokPriv), NULL, NULL) != 0;
  }
  return false;
}

DWORD getPID(const tstring &procName)
{
  HANDLE hSnap;
  PROCESSENTRY32W procEnt = {0};

  hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  procEnt.dwSize = sizeof(PROCESSENTRY32W);
  do {
    if (!wcscmp(procEnt.szExeFile, procName.c_str())) {
      CloseHandle(hSnap);
      return procEnt.th32ProcessID;
    }
  } while(Process32NextW(hSnap, &procEnt));
  CloseHandle(hSnap);
  return -1;
}

void inject(DWORD pid, const tstring &dllName, bool needDebug)
{
  SIZE_T wr;
  DWORD id;
  HANDLE hProc = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, false, pid);
  if (!hProc) {
    throw new exception("Can not open process");
  }
  BYTE *pCode = (BYTE *)VirtualAllocEx(hProc, NULL, sizeof(remCALL_PARAMETERS64), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
  if (pCode == nullptr) {
     throw new exception("Can not allocate virtual memory!");
  }

  remCALL_PARAMETERS64 cmd(needDebug);
  HMODULE hMod = GetModuleHandle(_T("kernel32.dll"));
  cmd.loadLibAddr = (intptr_t)GetProcAddress(hMod, "LoadLibraryA");
  cmd.exitThreadAddr = (intptr_t)GetProcAddress(hMod, "ExitThread");
  int maxBufLen = static_cast<int>(dllName.length());
  if (maxBufLen > DLLNAME_LEN) {
     throw new exception("DLL name too long");
  }
  char *nameBuf = new char[DLLNAME_LEN + 1];
  int strLen = WideCharToMultiByte(CP_ACP, 0, dllName.c_str(), maxBufLen + 1, nameBuf, DLLNAME_LEN, 0, 0);
  memcpy(cmd.libName, nameBuf, strLen);
  delete[] nameBuf;
  WriteProcessMemory(hProc, pCode, &cmd, sizeof(cmd), &wr);
  
  HANDLE hThread = CreateRemoteThread(hProc, NULL, 0,  (unsigned long (__stdcall *)(void *))pCode, NULL, 0, &id);
  WaitForSingleObject(hThread, INFINITE);
  VirtualFreeEx(hProc, pCode, sizeof(cmd), MEM_RELEASE);
  CloseHandle(hProc);
}
int _tmain(int argc, _TCHAR **argv)
{
  if (argc <= 1 || argc > 6) {
    printHelp();
    return 1;
  }
  bool needDebug = false;
  tstring processName;
  tstring dllName;
  tstring *curParam = nullptr;

  try {
    for (int i = 1; i < argc; ++i) {
      if (!wcscmp(argv[i], _T("-P"))) {
        curParam = &processName;
      } else if (!wcscmp(argv[i], _T("-D"))) {
        curParam = &dllName;
      } else if (!wcscmp(argv[i], _T("-DBG"))) {
        needDebug = true;
      }
      if (curParam) {
        if (i + 1 < argc) {
          *curParam = argv[i + 1];
          ++i;
        }
        curParam = nullptr;
      }
    }
    if (processName.length() > 0 && dllName.length() > 0) {
      if (!getPrivileges()) {
        throw new exception("Unable to get privileges");
      } else {
        DWORD pid = getPID(processName);
        if (pid == -1) {
          throw new exception("No such process");
        }
        inject(pid, dllName, needDebug);
      }
    } else {
      printHelp();
    }
  }
  catch (exception &ex)
  {
    printMsg(ex.what());
  }

  return 0;
}